import scipy.io
from math import exp
import numpy as np
import matplotlib.pyplot as plt


#################################################################
##      Activations and Derivitives
#################################################################
def tanh(z): return (exp(z)- exp(-z)) / (exp(z) + exp(-z))  # activation function
def tanhDerive(z): 1 - tanh(z)**2 # derivative of tahn

def sigmoid(z): return 1/(1 + exp(-z))  # activation function
def sigmoidDerive(z): return sigmoid(z) * (1-sigmoid(z))  # derivative of sigmoid - return x * (1 - x)


#################################################################
##      Etc
#################################################################
def loss(predictions, i): return 0.5*np.sum((y[i]-predictions) ** 2)


#################################################################
##      Passes: Forward Propogation and Back Propogation
#################################################################
def forwardPass(row):
    # We use reshape because numpy sometimes makes arrays (n, ) instead of (n, 1)
    data = row.reshape(len(row),1)
    for layer in network:
        preactivation = np.dot(layer['weights'].T, data) + layer['bias']
        layer['preactivation'] = np.array(preactivation)
        # Activation on neurons
        newout = []
        for j in preactivation:
            newout.append(activate(j))
        newout = np.array(newout).reshape(len(preactivation),1)
        # Add activations to layer
        layer['activations'] = newout
        # this is now our new input
        data = newout


def backPass(softm, correct, xxx, e=0.000001):
        # Book Keeping - gradients to return
        db = []
        dw = []

        ddb = [] # double check with Numerations
        ddw = [] # double check with Numerations

        # Find error: correct answer minus softmax probabilities
        delta = -np.subtract(correct, softm.T)

        # Loop backwards through network
        for i in reversed(range(len(network))):
            #Layer's activation function derivatives
            activation_prime = np.array([derive(z) for z in network[i]['preactivation']])

            network[i]['derive_weight'].append(delta * network[i-1]['activations'])
            network[i]['derive_b'].append(delta)

            dw.append(delta * network[i-1]['activations'])
            db.append(delta)

            # Hadamard Product
            dh = network[i]['weights'].T * delta
            delta = np.multiply(dh, activation_prime.T)


        ############################################################################
        ##      Numerical Differetiation
        ############################################################################
            # Purturb W's, calculate loss
            temp = np.zeros(shape=network[i]['weights'].shape)
            for wx in range(len(network[i]['weights'])):
                for wy in range(len(network[i]['weights'])):

                    bbb = network[i]['bias']

                    tempW = network[i]['weights']
                    tempWPlus = tempW[wx][wy] + e
                    tempWMinus = tempW[wx][wy] - e

                    l1 = loss((np.dot(tempWPlus, xxx) + bbb), i)
                    l2 = loss((np.dot(tempWMinus, xxx) + bbb), i)
                    temp[wx][wy] = ((l1-l2)/(2*e))
            network[i]['grad_check_weight'].append(temp)
            ddw.append(temp)

            # Purturb b's, calculate loss
            temp = np.zeros(shape=network[i]['bias'].shape)
            for bx in range(len(network[i]['bias'])):
                www = network[i]['weights']
                tempB = network[i]['bias']
                tempBPlus = tempB[bx] + e
                tempBMinus = tempB[bx] - e

                l1 = loss((np.dot(www, xxx) + tempBPlus), i)
                l2 = loss((np.dot(www, xxx) + tempBMinus), i)

                temp[bx] = ((l1-l2)/(2*e))
            network[i]['grad_check_b'].append(temp)
            ddb.append(temp)

        return dw, db, ddw, ddb


layers = 10
func = 'sigmoid'
activate = globals()[func]
derive = globals()[func + "Derive"]

p1 = scipy.io.loadmat('problem-1.mat')
'''
W: 10 x 1 NDArray
b: 10 x 1 NDArray
x: 100 x 10 NDArray
y: 100 x 10 NDArray
'''
w = p1['W']
b = p1['b']
x = p1['x']
y = p1['y']

cleanup = []
for i in range(len(w)):
    cleanup.append(w[i][0])
w = cleanup

cleanup = []
for i in range(len(b)):
    cleanup.append(b[i][0])
b = cleanup

#################################################################
##      Build Network
#################################################################
network = []
for layer in range(layers):
    network.append(
        {'func': activate, 'weights': w[layer], 'bias': b[layer], 'preactivation': [], 'activations': [],
         'derive_weight': [], 'derive_b': [], 'diff_weight': [], 'diff_b': [], 'grad_check_b': [],
         'grad_check_weight': []})

losses = []
# Do Things
for i in range(len(x)):
    forwardPass(x[i])
    sm = loss(network[-1]['preactivation'], i)
    losses.append(sm)
    dw, db, ddww, ddbb = backPass(sm, y[i], x[i])


#################################################################
##      Analyze Gradients
#################################################################
print("===============")
print("Forward pass output for last x: ")
for i in range(len(network)):
    print(network[i]['preactivation'][:, 0])


print('===============')
print("Backward pass output for last x: ")
for i in range(len(network)):
    print('weight: ' + str(network[i]['derive_weight'][0]))
    print('##')
    print('bias: ' + str(network[i]['derive_b'][0]))



print(np.linalg.norm(np.array(dw)-np.array(ddww)))
# print(np.linalg.norm(np.array(db)-np.array(ddbb)))
print(np.linalg.norm(np.subtract(dw, ddww))/np.linalg.norm(np.array(dw)+np.array(ddww)))
# print(np.linalg.norm(gcb-ggccbb)/np.linalg.norm(gcb+ggccbb))



plt.scatter(dw, ddww, color='blue')
# plt.xlabel('Gradient Approximations (Weights)')
# plt.ylabel('Gradient of Weights')

# plt.scatter(gcb, ggccbb)
plt.xlabel('Gradient Approximations (Weights)')
plt.ylabel('Gradient of Weights')
plt.show()