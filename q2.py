import time
import scipy.io
from math import exp
import numpy as np
import sys
from math import *
import matplotlib.pyplot as plt

#################################################################
##      Helper Functions
#################################################################

results = {
    'epoches' : 0,
    'loss_over_eps': [],
    'accuracy_over_eps': [],
    'time': [],
    'validation': []
}


def timing(f):
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        # print('%s function took %0.3f ms' % (f.__name__, (time2-time1)*1000.0))
        results['time'].append(time2-time1)
        return ret
    return wrap


def validation(predictions, actual):
    assert len(predictions) == len(actual)

    correct = 0.0
    for i in range(len(actual)):
        # decode to integers for comparison
        # print(str(np.argmax(predictions[i])) + ' : ' + str(np.argmax(actual[i])))
        if np.argmax(predictions[i]) == np.argmax(actual[i]):
            correct += 1
    return round(correct/len(actual)*100, 4)


def showResults(network, err):
    if network[0]['two']:
        outi = ['%.4f' % i for i in results['time']]
        print('Time in seconds: ' + str(outi))
        print(results['loss_over_eps'])

        plt.plot(range(results['epoches']), results['loss_over_eps'])
        plt.show()

        plt.plot(range(results['epoches']), results['accuracy_over_eps'])
        plt.show()

    else:
        print("================ Output for Network using " + str(network[0]['func'].__name__))
        print("### accumulated error: " + str(round(err, 4)))
        print(results['time'])

        for i in range(len(network)):
            layer = network[i]
            print('\n ********** Layer ' + str(i+1))
            outi = ['%.4f' % i for i in layer['activations']]
            print('Forward Pass: ' + str(outi))

            # grad = ['%.4f' % i for i in layer['derive_weight']]
            grad = layer['derive_weight']
            print('Gradient-Weights: ' + str(grad))

        bias = layer['derive_b']
        print('Gradient-Bias: ' + str(bias))


#################################################################
##      Activations and Derivitives
#################################################################
def tahn(z): return (exp(z)- exp(-z)) / (exp(z) + exp(-z))  # activation function
def tahnDerive(z): 1 - (tahn(z))**2 # derivative of tahn

def sigmoid (z): return 1/(1 + exp(-z))  # activation function
def sigmoidDerive(z): return sigmoid(z) * (1-sigmoid(z))  # derivative of sigmoid - return x * (1 - x)


def relu(z): return np.maximum(0.0001, z)
def reluDerive(z):
    z[z <= 0.0] = 0.0
    z[z > 0.0] = 1.0
    return z

def softmax(z): return np.exp((z-np.max(z))+0.0001) / (np.sum(np.exp(z-np.max(z)))+0.0001)

#################################################################
##      Optimizers
#################################################################

def update_w_b(opt='sgd'):

    lr = 0.1  # learning rate
    m = len(network[0]['derive_b']) # Batch size - moved outside for loop to reduce redundant calculations

    momentum = 0.1
    velocity = 0.1
    decay = 0.1

    #Adagrad
    r_w = 0.0
    r_b = 0.0

    #Adadelta
    T = 10000 # Steps

    for i in range(len(network)):
        biases_gradiants = network[i]['derive_b']
        weights_gradiants = network[i]['derive_weight']
        layer = network[i]

        # Compute gradients
        G_w = lr * (1 / m) * np.sum(weights_gradiants)
        G_b = lr * (1 / m) * np.sum(biases_gradiants)


        if opt == 'sgd':
            layer['bias'] -= G_b
            layer['weights'] -= G_w

        elif opt =='sgd-nest':
            # Compute Velocity Update: v = momentum*velocity - lr*G
            velocity_b = momentum * velocity - G_b
            velocity_w = momentum*velocity - G_w

            # apply update: theta += velocity
            layer['weights'] += velocity_w
            layer['bias'] += velocity_b

        elif opt =='adaGrad':
            # Accumulate squared gradient: r += r+ G dot G
            r_w += r_w + np.dot(G_w, G_w)
            r_b += r_b + np.dot(G_b, G_b)

            # Compute update: delta theta = (- lr /(e + sqrt(r))) dot G [div and sqrt applied element wise]
            delta_w = np.dot((-lr * sqrt(r_w)+ e + sqrt(r_w)), G_w)
            delta_b = np.dot((-lr * sqrt(r_b) + e + sqrt(r_b)), G_b)

            # Apply update: theta += delta theta
            layer['weights'] += delta_w
            layer['bias'] += delta_b

        elif opt == 'adaDelta':
            x = 0
            # for x in weights_gradiants:
            #     for gradient in x:
            #         # init ialize accumulation variables E[g**2]0 = 0; E[delta x **2}0 = 0
            #         mean_gradient2 = 0
            #         mean_step2 = 0
            #         steps = np.zeros(T)
            #         # for t = 1:T do %% Loop over # of updates
            #         for t in range(T-1):
            #             mean_gradient2 = decay * mean_gradient2 + (1 - decay) * gradient ** 2
            #             steps[i] = ((mean_step2 + e) / (mean_gradient2 + e)) ** 0.5 * gradient
            #             mean_step2 = decay * mean_step2 + (1 - decay) * steps[i] ** 2
            #
            #             layer['weights'] += mean_step2
            #
            # for gradient in biases_gradiants:
            # # init ialize accumulation variables E[g**2]0 = 0; E[delta x **2}0 = 0
            #     mean_gradient2 = 0
            #     mean_step2 = 0
            #     steps = np.zeros(T)
            #
            #     # for t = 1:T do %% Loop over # of updates
            #     for t in range(T - 1):
            #         mean_gradient2 = decay * mean_gradient2 + (1 - decay) * gradient ** 2
            #         steps[i] = ((mean_step2 + e) / (mean_gradient2 + e)) ** 0.5 * gradient
            #         mean_step2 = decay * mean_step2 + (1 - decay) * steps[i] ** 2
            #
            #         layer['bias'] += mean_step2
            #         # # Compute gradient gt:
            #         # # Accumulate Gradient: E[g**2]t = decay*E[g**2]t-1 + (1-decay)* gt**2
            #         # eg2[t] = decay * eg2[t-1]+ (1 - decay) * gradient ** 2
            #         #
            #         # # Compute update: delta x t = -(RMS[delta x]t-1) / (RMS[g]t) * gt #Root Mean Square
            #         # delta_x = - np.sqrt(np.mean(np.square(dx2))) / np.sqrt(np.mean(np.square(gradient))) * G_w
            #         #
            #         # # Accumulate Updates: E[delta x **2]t = decay * E[delta x **2]t-1 + (1-decay)* delta x **2
            #         # dx2 = decay * dx2 + (1-decay)* delta_x**2
            #     # Apply update: xt+1 = xt + delta x t

#################################################################
##      Building network
#################################################################
def forwardPass(row):
    # We use reshape because numpy sometimes makes arrays (n, ) instead of (n, 1)
    data = row.reshape(len(row),1)

    for layer in network:
        preactivation = np.dot(layer['weights'], data) + layer['bias']
        layer['preactivation'] = np.array(preactivation)

        # Activation on neurons
        newout = []
        for j in range(len(preactivation)):
            newout.append(activate(preactivation[j]))
        newout = np.array(newout).reshape(len(preactivation),1)

        # Add activations to layer
        layer['activations'] = newout

        # this is now our new input
        data = newout


def backPass(softm, correct):
    # Book Keeping - gradients to return
    db = []
    dw = []

    # Loop backwards through network
    for i in reversed(range(len(network))):
        layer = network[i]
        dl = []

        # calculate relu derivatives for each layer's activations
        for neuron in layer['preactivation'].T:
            dl.append(derive(neuron))

        # Find error: correct answer minus softmax probabilities
        if i == len(network)-1:
            delta = -np.add(correct, softm.T)

        # Back Propagate error
        else:
            previous_layer = network[i + 1]

            network[i]['derive_weight'].append(np.dot(layer['activations'], delta))
            network[i]['derive_b'].append(delta)
            dh = np.dot(delta, previous_layer['weights'])

            # Hadamard Product
            delta = np.multiply(dh, dl)

            db.append(network[i]['derive_b'])
            dw.append(network[i]['derive_weight'])
    return dw, db

def loss(predictions, actual):
    if network[0]['two']:  # Cross-Entropy
        lo = 0.0
        for i in range(len(predictions)):
            lo += np.log(predictions[i] + 0.0001)
        return -lo

    else:
        return np.sum((actual - predictions) ** 2)


@timing
def train(training_set, training_labels, optimizer, batch_size=32, epochs=100):
    results['epoches'] = epochs
    epoch_losses =  []
    epoch_accuracies = []

    for epoch in range(epochs):
        np.random.shuffle(training_set)
        batches = [training_set[k:k + batch_size] for k in range(0, len(training_set), batch_size)]
        batch_labels = [training_labels[k:k + batch_size] for k in range(0, len(training_labels), batch_size)]
        accuracies = []

        for i in range(len(batches)):
            batch = batches[i]
            labels = batch_labels[i]

            # keep track of info for end of batch calculations
            losses=[]
            batch_softmaxes = []


            for row in range(len(batch)):
                # Forward Pass and calculate activations
                forwardPass(batch[row])

                # Make prediction
                if network[0]['two']:
                    guess = softmax(network[-1]['preactivation'])
                else:
                    guess = network[-1]['preactivation']*network[-1]['weights'] + network[-1]['bias']

                # Book Keeping
                accuracies.append(validation(guess.T, y[row]))
                batch_softmaxes.append(guess)

                # Back Pass and calculate the gradients
                backPass(guess, labels[row])

            #### END OF SINGLE BATCH LOOP: Move onto next batch
            # if question 2, update weights and bias
            if network[0]['two']:
                update_w_b(optimizer)

            losses.append(loss(batch_softmaxes, labels))

        print("Accuracy: " + str(np.average(accuracies)))
        print("Accuracy-max: " + str(np.max(accuracies)))
        epoch_losses.append(np.average(losses))
        epoch_accuracies.append(np.average(accuracies))
    results['loss_over_eps'].append(epoch_losses)
    results['accuracy_over_eps'].append()





#################################################################
##      Main
#################################################################
if __name__ == "__main__":
    print("\n \n Welcome to Homework 2 part 2: 5 layer network with ReLu Activations")
    layers = 5
    func = "relu"
    two = True

    '''
    images: 60000, 28, 28
    labels: 60000, 1
    '''
    train_set = scipy.io.loadmat('mnistTrain.mat')
    test_set = scipy.io.loadmat('mnistTest.mat')

    x = train_set['images'].reshape(60000, 784)
    # rescale to -1 and 1
    x = (2*(x-np.min(x))/(np.max(x)-np.min(x)))-1


    # convert to one hot encoding
    y = np.eye(np.max(train_set['labels']) + 1)[train_set['labels']]


    x_test = test_set['images'].reshape(10000, 784)
    y_test = np.eye(np.max(test_set['labels']) + 1)[test_set['labels']]

    w = [np.random.uniform(-1, high=1, size=(32,784))]
    for i in range(layers-1):
        w.append(np.random.uniform(-1, high=1, size=(32,32)))
    w.append(np.random.uniform(-1, high=1, size=(10, 32)))
    # replace 0's with epsilon
    w = np.where(w==0, 0.0001, w)


    b = []
    for i in range(layers):
        b.append(np.random.random((32,1)))
    b.append(np.random.random((10, 1)))


    funcderive = func + "Derive"
    activate = globals()[func]
    derive = globals()[funcderive]

    ##################### set up network
    # Layer dictionary:
    # func: which activation/gradient to use (i.e. sigmoid, tahn, relu)
    # weights: layer's weights
    # bias: layer's bias
    # activations: output of activation((w*x)+b)
    # derive_weight: cost derivative with respect to weight
    # derive_b: cost derivative with respect to bias

    indiv_result = []
    network = []
    for layer in range(layers+1):
        network.append({'two': two, 'func': activate, 'weights': w[layer], 'bias': b[layer], 'preactivation': [], 'activations': [], 'derive_weight':[], 'derive_b':[]})

    optimizers = ['sgd', 'sgd-nest', 'adaGrad', 'adaDelta']

    for optimizer in optimizers:
        err = train(x, y, optimizer, 32, 5)
        print(str(optimizer) +': ' + str(results['time']))
        # Training Results
        # showResults(err)

        # Validation Results
        validation_predictions = []
        for i in range(len(x_test)):
            forwardPass(x_test[i], network, activate)
            validation_predictions.append(softmax(network[-2]['activations']))
        results['validation'].append((validation(validation_predictions, y_test)))

        indiv_result.append(results)
        results = {
            'epoches': 0,
            'loss_over_eps': [],
            'accuracy_over_eps': [],
            'time': [],
            'validation': []
        }

        # Save Results
        import pickle
        pickle.dump(indiv_result, open('results.p', 'wb'))
